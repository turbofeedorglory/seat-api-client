<?php
/**
 * CorporationWalletTransaction
 *
 * PHP version 7.3
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * SeAT API
 *
 * SeAT API Documentation. All endpoints require an API key.
 *
 * The version of the OpenAPI document: 2.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.3.1-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace OpenAPI\Client\Model;

use \ArrayAccess;
use \OpenAPI\Client\ObjectSerializer;

/**
 * CorporationWalletTransaction Class Doc Comment
 *
 * @category Class
 * @description Corporation Wallet Transaction
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<TKey, TValue>
 * @template TKey int|null
 * @template TValue mixed|null
 */
class CorporationWalletTransaction implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'CorporationWalletTransaction';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'division' => 'int',
        'transaction_id' => 'int',
        'date' => '\DateTime',
        'location_id' => 'int',
        'unit_price' => 'double',
        'quantity' => 'int',
        'is_buy' => 'bool',
        'journal_ref_id' => 'int',
        'party' => '\OpenAPI\Client\Model\UniverseName',
        'type' => '\OpenAPI\Client\Model\InvType'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'division' => null,
        'transaction_id' => 'int64',
        'date' => 'date-time',
        'location_id' => 'int64',
        'unit_price' => 'double',
        'quantity' => null,
        'is_buy' => null,
        'journal_ref_id' => 'int64',
        'party' => null,
        'type' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'division' => 'division',
        'transaction_id' => 'transaction_id',
        'date' => 'date',
        'location_id' => 'location_id',
        'unit_price' => 'unit_price',
        'quantity' => 'quantity',
        'is_buy' => 'is_buy',
        'journal_ref_id' => 'journal_ref_id',
        'party' => 'party',
        'type' => 'type'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'division' => 'setDivision',
        'transaction_id' => 'setTransactionId',
        'date' => 'setDate',
        'location_id' => 'setLocationId',
        'unit_price' => 'setUnitPrice',
        'quantity' => 'setQuantity',
        'is_buy' => 'setIsBuy',
        'journal_ref_id' => 'setJournalRefId',
        'party' => 'setParty',
        'type' => 'setType'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'division' => 'getDivision',
        'transaction_id' => 'getTransactionId',
        'date' => 'getDate',
        'location_id' => 'getLocationId',
        'unit_price' => 'getUnitPrice',
        'quantity' => 'getQuantity',
        'is_buy' => 'getIsBuy',
        'journal_ref_id' => 'getJournalRefId',
        'party' => 'getParty',
        'type' => 'getType'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['division'] = $data['division'] ?? null;
        $this->container['transaction_id'] = $data['transaction_id'] ?? null;
        $this->container['date'] = $data['date'] ?? null;
        $this->container['location_id'] = $data['location_id'] ?? null;
        $this->container['unit_price'] = $data['unit_price'] ?? null;
        $this->container['quantity'] = $data['quantity'] ?? null;
        $this->container['is_buy'] = $data['is_buy'] ?? null;
        $this->container['journal_ref_id'] = $data['journal_ref_id'] ?? null;
        $this->container['party'] = $data['party'] ?? null;
        $this->container['type'] = $data['type'] ?? null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets division
     *
     * @return int|null
     */
    public function getDivision()
    {
        return $this->container['division'];
    }

    /**
     * Sets division
     *
     * @param int|null $division Wallet key of the division to fetch journals from
     *
     * @return self
     */
    public function setDivision($division)
    {
        $this->container['division'] = $division;

        return $this;
    }

    /**
     * Gets transaction_id
     *
     * @return int|null
     */
    public function getTransactionId()
    {
        return $this->container['transaction_id'];
    }

    /**
     * Sets transaction_id
     *
     * @param int|null $transaction_id Unique transaction ID
     *
     * @return self
     */
    public function setTransactionId($transaction_id)
    {
        $this->container['transaction_id'] = $transaction_id;

        return $this;
    }

    /**
     * Gets date
     *
     * @return \DateTime|null
     */
    public function getDate()
    {
        return $this->container['date'];
    }

    /**
     * Sets date
     *
     * @param \DateTime|null $date Date and time of transaction
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->container['date'] = $date;

        return $this;
    }

    /**
     * Gets location_id
     *
     * @return int|null
     */
    public function getLocationId()
    {
        return $this->container['location_id'];
    }

    /**
     * Sets location_id
     *
     * @param int|null $location_id The place where the transaction has been made
     *
     * @return self
     */
    public function setLocationId($location_id)
    {
        $this->container['location_id'] = $location_id;

        return $this;
    }

    /**
     * Gets unit_price
     *
     * @return double|null
     */
    public function getUnitPrice()
    {
        return $this->container['unit_price'];
    }

    /**
     * Sets unit_price
     *
     * @param double|null $unit_price Amount paid per unit
     *
     * @return self
     */
    public function setUnitPrice($unit_price)
    {
        $this->container['unit_price'] = $unit_price;

        return $this;
    }

    /**
     * Gets quantity
     *
     * @return int|null
     */
    public function getQuantity()
    {
        return $this->container['quantity'];
    }

    /**
     * Sets quantity
     *
     * @param int|null $quantity quantity
     *
     * @return self
     */
    public function setQuantity($quantity)
    {
        $this->container['quantity'] = $quantity;

        return $this;
    }

    /**
     * Gets is_buy
     *
     * @return bool|null
     */
    public function getIsBuy()
    {
        return $this->container['is_buy'];
    }

    /**
     * Sets is_buy
     *
     * @param bool|null $is_buy True if the transaction is related to a buy order
     *
     * @return self
     */
    public function setIsBuy($is_buy)
    {
        $this->container['is_buy'] = $is_buy;

        return $this;
    }

    /**
     * Gets journal_ref_id
     *
     * @return int|null
     */
    public function getJournalRefId()
    {
        return $this->container['journal_ref_id'];
    }

    /**
     * Sets journal_ref_id
     *
     * @param int|null $journal_ref_id -1 if there is no corresponding wallet journal entry
     *
     * @return self
     */
    public function setJournalRefId($journal_ref_id)
    {
        $this->container['journal_ref_id'] = $journal_ref_id;

        return $this;
    }

    /**
     * Gets party
     *
     * @return \OpenAPI\Client\Model\UniverseName|null
     */
    public function getParty()
    {
        return $this->container['party'];
    }

    /**
     * Sets party
     *
     * @param \OpenAPI\Client\Model\UniverseName|null $party party
     *
     * @return self
     */
    public function setParty($party)
    {
        $this->container['party'] = $party;

        return $this;
    }

    /**
     * Gets type
     *
     * @return \OpenAPI\Client\Model\InvType|null
     */
    public function getType()
    {
        return $this->container['type'];
    }

    /**
     * Sets type
     *
     * @param \OpenAPI\Client\Model\InvType|null $type type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->container['type'] = $type;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}



# OpenAPIClient-php

SeAT API Documentation. All endpoints require an API key.


## Installation & Usage

### Requirements

PHP 7.3 and later.
Should also work with PHP 8.0 but has not been tested.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/GIT_USER_ID/GIT_REPO_ID.git"
    }
  ],
  "require": {
    "GIT_USER_ID/GIT_REPO_ID": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\AssetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$corporation_id = 56; // int | Corporation id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->call4c8adfa8ef264474d5d6bdcb07a08e61($corporation_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AssetsApi->call4c8adfa8ef264474d5d6bdcb07a08e61: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *https://seat.services.wormholerbtw.space/api*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AssetsApi* | [**call4c8adfa8ef264474d5d6bdcb07a08e61**](docs/Api/AssetsApi.md#call4c8adfa8ef264474d5d6bdcb07a08e61) | **GET** /v2/corporation/assets/{corporation_id} | Get a paginated list of a assets for a corporation
*AssetsApi* | [**call9cf9b486e75de62ca10b96cf44bc72be**](docs/Api/AssetsApi.md#call9cf9b486e75de62ca10b96cf44bc72be) | **GET** /v2/character/assets/{character_id} | Get a paginated list of a assets for a character
*CharacterApi* | [**adad9bd5091da85471977d8dc17239e6**](docs/Api/CharacterApi.md#adad9bd5091da85471977d8dc17239e6) | **GET** /v2/character/jump-clones/{character_id} | Get a paginated list of jump clones for a character
*CharacterApi* | [**adf0d485f7c96327108d9aad6996c1dd**](docs/Api/CharacterApi.md#adf0d485f7c96327108d9aad6996c1dd) | **GET** /v2/character/notifications/{character_id} | Get a paginated list of notifications for a character
*CharacterApi* | [**call0dc54bd4de18e00e8d3cb04f9132ffc0**](docs/Api/CharacterApi.md#call0dc54bd4de18e00e8d3cb04f9132ffc0) | **GET** /v2/character/sheet/{character_id} | Get the character sheet for a character
*CharacterApi* | [**call3d43bc5a8d462e1a335f687a9f20ff2e**](docs/Api/CharacterApi.md#call3d43bc5a8d462e1a335f687a9f20ff2e) | **GET** /v2/character/mail/{character_id} | Get a paginated list of mail for a character
*CharacterApi* | [**cea7975e72cc2678840f5bcfc20aa45c**](docs/Api/CharacterApi.md#cea7975e72cc2678840f5bcfc20aa45c) | **GET** /v2/character/skill-queue/{character_id} | Get a list of characters skill queue
*CharacterApi* | [**cf85d2d86997fa5650cc4e927d3283ee**](docs/Api/CharacterApi.md#cf85d2d86997fa5650cc4e927d3283ee) | **GET** /v2/character/corporation-history/{character_id} | Get the corporation history for a character
*CharacterApi* | [**f71b7adb5274009dad7792fe95fb7e07**](docs/Api/CharacterApi.md#f71b7adb5274009dad7792fe95fb7e07) | **GET** /v2/character/skills/{character_id} | Get the skills for a character
*ContactsApi* | [**a6bb00dd1bed3f6e63b4e02c1c54ec22**](docs/Api/ContactsApi.md#a6bb00dd1bed3f6e63b4e02c1c54ec22) | **GET** /v2/character/contacts/{character_id} | Get a paginated list of contacts for a character
*ContactsApi* | [**e6c013c5d20323ce7e623efa9e00f0d7**](docs/Api/ContactsApi.md#e6c013c5d20323ce7e623efa9e00f0d7) | **GET** /v2/corporation/contacts/{corporation_id} | Get a list of contacts for a corporation
*ContractsApi* | [**call4d3c61c1fb4e8281c4e4b02aab77feec**](docs/Api/ContractsApi.md#call4d3c61c1fb4e8281c4e4b02aab77feec) | **GET** /v2/corporation/contracts/{corporation_id} | Get a paginated list of contracts for a corporation
*ContractsApi* | [**call76a3daa992b27d8522688f16deac819e**](docs/Api/ContractsApi.md#call76a3daa992b27d8522688f16deac819e) | **GET** /v2/character/contracts/{character_id} | Get a paginated list of contracts for a character
*CorporationApi* | [**call790d0d2149ca67ae7d0dc216be53baef**](docs/Api/CorporationApi.md#call790d0d2149ca67ae7d0dc216be53baef) | **GET** /v2/corporation/member-tracking/{corporation_id} | Get a list of members for a corporation with tracking
*CorporationApi* | [**call80868b7914d1aab73c81ddd356db45d5**](docs/Api/CorporationApi.md#call80868b7914d1aab73c81ddd356db45d5) | **GET** /v2/corporation/sheet/{corporation_id} | Get a corporation sheet
*IndustryApi* | [**call7338af63f38cdbfa0e6634f882758ebe**](docs/Api/IndustryApi.md#call7338af63f38cdbfa0e6634f882758ebe) | **GET** /v2/corporation/industry/{corporation_id} | Get a paginated list of industry jobs for a corporation
*IndustryApi* | [**df86905de68b4f55a7c767a07adf5692**](docs/Api/IndustryApi.md#df86905de68b4f55a7c767a07adf5692) | **GET** /v2/character/industry/{character_id} | Get a paginated list of industry jobs for a character
*KillmailsApi* | [**b8b8ef3f29bd5951ff2def26be4f002d**](docs/Api/KillmailsApi.md#b8b8ef3f29bd5951ff2def26be4f002d) | **GET** /v2/killmails/{killmail_id} | Get full details about a killmail
*KillmailsApi* | [**call17814a7d2c1a6bb1dbe0336d41573e00**](docs/Api/KillmailsApi.md#call17814a7d2c1a6bb1dbe0336d41573e00) | **GET** /v2/corporation/killmails/{corporation_id} | Get a paginated list of killmails for a corporation
*KillmailsApi* | [**call828598394baf5376b30bb857d49f2ead**](docs/Api/KillmailsApi.md#call828598394baf5376b30bb857d49f2ead) | **GET** /v2/character/killmails/{character_id} | Get a paginated list of killmails for a character
*MarketApi* | [**call60013c0c86ff76731bb02be3eab5881d**](docs/Api/MarketApi.md#call60013c0c86ff76731bb02be3eab5881d) | **GET** /v2/corporation/market-orders/{corporation_id} | Get a paginated list of market orders for a corporation
*MarketApi* | [**call6e92a0a7c4b83dd5ace6dbf3953ae353**](docs/Api/MarketApi.md#call6e92a0a7c4b83dd5ace6dbf3953ae353) | **GET** /v2/character/market-orders/{character_id} | Get a paginated list of market orders for a character
*RolesApi* | [**a9c2d03b01bc2138f0826b23f5a980ca**](docs/Api/RolesApi.md#a9c2d03b01bc2138f0826b23f5a980ca) | **PATCH** /v2/roles/{role_id} | Edit an existing SeAT role
*RolesApi* | [**b9951e9b42d41515a9bf73bd43bc5ab7**](docs/Api/RolesApi.md#b9951e9b42d41515a9bf73bd43bc5ab7) | **POST** /v2/roles | Create a new SeAT role
*RolesApi* | [**call559745b6fccf8de1570962d8c492f229**](docs/Api/RolesApi.md#call559745b6fccf8de1570962d8c492f229) | **DELETE** /v2/roles/members/{user_id}/{role_id} | Revoke a SeAT role from an user
*RolesApi* | [**call789426941aed7d332b63ff5f9a1d3e77**](docs/Api/RolesApi.md#call789426941aed7d332b63ff5f9a1d3e77) | **DELETE** /v2/roles/{role_id} | Delete a SeAT role
*RolesApi* | [**call85121e5fae265f8c66a39d3f47441a59**](docs/Api/RolesApi.md#call85121e5fae265f8c66a39d3f47441a59) | **GET** /v2/roles | Get the roles configured within SeAT
*RolesApi* | [**call92356addbf79c2a7d3a6e376adfc05b4**](docs/Api/RolesApi.md#call92356addbf79c2a7d3a6e376adfc05b4) | **GET** /v2/roles/{role_id} | Get detailed information about a role
*RolesApi* | [**e7597dfde6abe556b8f20f4f4f7f5ea1**](docs/Api/RolesApi.md#e7597dfde6abe556b8f20f4f4f7f5ea1) | **POST** /v2/roles/members | Grant a user a SeAT role
*RolesApi* | [**ea8d2bdbf03712cf2eedee524a9c831f**](docs/Api/RolesApi.md#ea8d2bdbf03712cf2eedee524a9c831f) | **GET** /v2/roles/query/permission-check/{user_id}/{permission_name} | Check if a user has a role
*RolesApi* | [**f47790627b1f040eae7e92a89f17dfc5**](docs/Api/RolesApi.md#f47790627b1f040eae7e92a89f17dfc5) | **GET** /v2/roles/query/permissions | Get the available SeAT permissions
*RolesApi* | [**f57ba37792c2fcbf0e7805134528ac80**](docs/Api/RolesApi.md#f57ba37792c2fcbf0e7805134528ac80) | **GET** /v2/roles/query/role-check/{user_id}/{role_name} | Check if a user has a role
*SquadsApi* | [**call37f22499ac5a50ccaffbd6427863e402**](docs/Api/SquadsApi.md#call37f22499ac5a50ccaffbd6427863e402) | **POST** /v2/squads/ | Create a new SeAT squad
*SquadsApi* | [**call61888b93ce0ecc350c7969bd702f866f**](docs/Api/SquadsApi.md#call61888b93ce0ecc350c7969bd702f866f) | **DELETE** /v2/squads/{squad_id} | Delete a SeAT squad
*SquadsApi* | [**call81efae22f6e7b401fc434f2f30a0f139**](docs/Api/SquadsApi.md#call81efae22f6e7b401fc434f2f30a0f139) | **GET** /v2/squads/{squad_id} | Get details about a Squad
*SquadsApi* | [**eb59ebfbde482a6d8d38dc933fa25a54**](docs/Api/SquadsApi.md#eb59ebfbde482a6d8d38dc933fa25a54) | **GET** /v2/squads | Get a list of squads
*UsersApi* | [**baf810a249f09694192dc4b5b4839017**](docs/Api/UsersApi.md#baf810a249f09694192dc4b5b4839017) | **GET** /v2/users/{user_id} | Get group id&#39;s and associated character_id&#39;s for a user
*UsersApi* | [**call44cb5dfd60bab9966857cbe3af9aff7a**](docs/Api/UsersApi.md#call44cb5dfd60bab9966857cbe3af9aff7a) | **GET** /v2/users/configured-scopes | Get a list of the scopes configured for this instance
*UsersApi* | [**call844c004c9f6492e74e1e4058340d0503**](docs/Api/UsersApi.md#call844c004c9f6492e74e1e4058340d0503) | **GET** /v2/users | Get a list of users, associated character id&#39;s and group ids
*UsersApi* | [**call85aeb7c16b3694741cfc93d128e7502c**](docs/Api/UsersApi.md#call85aeb7c16b3694741cfc93d128e7502c) | **POST** /v2/users/ | Create a new SeAT user
*UsersApi* | [**e422f75fa6436602f7639ff2dad5bf7a**](docs/Api/UsersApi.md#e422f75fa6436602f7639ff2dad5bf7a) | **DELETE** /v2/users/{user_id} | Delete a SeAT user
*WalletApi* | [**call52705c999d0f698826008008d3cd5e52**](docs/Api/WalletApi.md#call52705c999d0f698826008008d3cd5e52) | **GET** /v2/character/wallet-journal/{character_id} | Get a paginated wallet journal for a character
*WalletApi* | [**ec863a5097acd47cf0dbd89eb95f2d66**](docs/Api/WalletApi.md#ec863a5097acd47cf0dbd89eb95f2d66) | **GET** /v2/corporation/wallet-journal/{corporation_id} | Get a paginated wallet journal for a corporation
*WalletApi* | [**f821e55ea12d5251adb90b240c8de8ca**](docs/Api/WalletApi.md#f821e55ea12d5251adb90b240c8de8ca) | **GET** /v2/corporation/wallet-transactions/{corporation_id} | Get paginated wallet transactions for a corporation
*WalletApi* | [**fffdcea91aa35b1ec912355a4a7f312d**](docs/Api/WalletApi.md#fffdcea91aa35b1ec912355a4a7f312d) | **GET** /v2/character/wallet-transactions/{character_id} | Get paginated wallet transactions for a character

## Models

- [AllianceContact](docs/Model/AllianceContact.md)
- [CharacterAsset](docs/Model/CharacterAsset.md)
- [CharacterContact](docs/Model/CharacterContact.md)
- [CharacterCorporationHistory](docs/Model/CharacterCorporationHistory.md)
- [CharacterIndustryJob](docs/Model/CharacterIndustryJob.md)
- [CharacterJumpClone](docs/Model/CharacterJumpClone.md)
- [CharacterNotification](docs/Model/CharacterNotification.md)
- [CharacterOrder](docs/Model/CharacterOrder.md)
- [CharacterSheetResource](docs/Model/CharacterSheetResource.md)
- [CharacterSheetResourceSkillpoints](docs/Model/CharacterSheetResourceSkillpoints.md)
- [CharacterSkill](docs/Model/CharacterSkill.md)
- [CharacterSkillQueue](docs/Model/CharacterSkillQueue.md)
- [CharacterWalletJournal](docs/Model/CharacterWalletJournal.md)
- [CharacterWalletTransaction](docs/Model/CharacterWalletTransaction.md)
- [ContractBid](docs/Model/ContractBid.md)
- [ContractDetail](docs/Model/ContractDetail.md)
- [ContractItem](docs/Model/ContractItem.md)
- [CorporationAsset](docs/Model/CorporationAsset.md)
- [CorporationContact](docs/Model/CorporationContact.md)
- [CorporationIndustryJob](docs/Model/CorporationIndustryJob.md)
- [CorporationInfo](docs/Model/CorporationInfo.md)
- [CorporationMemberTracking](docs/Model/CorporationMemberTracking.md)
- [CorporationOrder](docs/Model/CorporationOrder.md)
- [CorporationWalletJournal](docs/Model/CorporationWalletJournal.md)
- [CorporationWalletTransaction](docs/Model/CorporationWalletTransaction.md)
- [CreateRole](docs/Model/CreateRole.md)
- [CreateRoleAllOf](docs/Model/CreateRoleAllOf.md)
- [InlineObject](docs/Model/InlineObject.md)
- [InlineObject1](docs/Model/InlineObject1.md)
- [InlineObject2](docs/Model/InlineObject2.md)
- [InlineObject3](docs/Model/InlineObject3.md)
- [InlineObject4](docs/Model/InlineObject4.md)
- [InlineResponse200](docs/Model/InlineResponse200.md)
- [InlineResponse2001](docs/Model/InlineResponse2001.md)
- [InlineResponse20010](docs/Model/InlineResponse20010.md)
- [InlineResponse20011](docs/Model/InlineResponse20011.md)
- [InlineResponse20012](docs/Model/InlineResponse20012.md)
- [InlineResponse20013](docs/Model/InlineResponse20013.md)
- [InlineResponse20014](docs/Model/InlineResponse20014.md)
- [InlineResponse20015](docs/Model/InlineResponse20015.md)
- [InlineResponse20016](docs/Model/InlineResponse20016.md)
- [InlineResponse20017](docs/Model/InlineResponse20017.md)
- [InlineResponse20018](docs/Model/InlineResponse20018.md)
- [InlineResponse20019](docs/Model/InlineResponse20019.md)
- [InlineResponse2002](docs/Model/InlineResponse2002.md)
- [InlineResponse20020](docs/Model/InlineResponse20020.md)
- [InlineResponse20021](docs/Model/InlineResponse20021.md)
- [InlineResponse20022](docs/Model/InlineResponse20022.md)
- [InlineResponse20023](docs/Model/InlineResponse20023.md)
- [InlineResponse20023Data](docs/Model/InlineResponse20023Data.md)
- [InlineResponse20024](docs/Model/InlineResponse20024.md)
- [InlineResponse20025](docs/Model/InlineResponse20025.md)
- [InlineResponse20026](docs/Model/InlineResponse20026.md)
- [InlineResponse20027](docs/Model/InlineResponse20027.md)
- [InlineResponse20028](docs/Model/InlineResponse20028.md)
- [InlineResponse20029](docs/Model/InlineResponse20029.md)
- [InlineResponse2003](docs/Model/InlineResponse2003.md)
- [InlineResponse20030](docs/Model/InlineResponse20030.md)
- [InlineResponse20031](docs/Model/InlineResponse20031.md)
- [InlineResponse2004](docs/Model/InlineResponse2004.md)
- [InlineResponse2005](docs/Model/InlineResponse2005.md)
- [InlineResponse2006](docs/Model/InlineResponse2006.md)
- [InlineResponse2007](docs/Model/InlineResponse2007.md)
- [InlineResponse2008](docs/Model/InlineResponse2008.md)
- [InlineResponse2009](docs/Model/InlineResponse2009.md)
- [InlineResponse201](docs/Model/InlineResponse201.md)
- [InlineResponse422](docs/Model/InlineResponse422.md)
- [InlineResponse422Errors](docs/Model/InlineResponse422Errors.md)
- [InvType](docs/Model/InvType.md)
- [Killmail](docs/Model/Killmail.md)
- [KillmailAttacker](docs/Model/KillmailAttacker.md)
- [KillmailDetail](docs/Model/KillmailDetail.md)
- [KillmailVictim](docs/Model/KillmailVictim.md)
- [MailHeader](docs/Model/MailHeader.md)
- [MailRecipient](docs/Model/MailRecipient.md)
- [MailResource](docs/Model/MailResource.md)
- [MapDenormalize](docs/Model/MapDenormalize.md)
- [PermissionResource](docs/Model/PermissionResource.md)
- [RefreshToken](docs/Model/RefreshToken.md)
- [ResourcePaginatedLinks](docs/Model/ResourcePaginatedLinks.md)
- [ResourcePaginatedMetadata](docs/Model/ResourcePaginatedMetadata.md)
- [Role](docs/Model/Role.md)
- [RoleResource](docs/Model/RoleResource.md)
- [Squad](docs/Model/Squad.md)
- [SquadAllOf](docs/Model/SquadAllOf.md)
- [SquadResource](docs/Model/SquadResource.md)
- [UniverseName](docs/Model/UniverseName.md)
- [UniverseStructure](docs/Model/UniverseStructure.md)
- [User](docs/Model/User.md)

## Authorization

### ApiKeyAuth

- **Type**: API key
- **API key parameter name**: X-Token
- **Location**: HTTP header


## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author



## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `2.0.0`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`

# OpenAPI\Client\ContactsApi

All URIs are relative to https://seat.services.wormholerbtw.space/api.

Method | HTTP request | Description
------------- | ------------- | -------------
[**a6bb00dd1bed3f6e63b4e02c1c54ec22()**](ContactsApi.md#a6bb00dd1bed3f6e63b4e02c1c54ec22) | **GET** /v2/character/contacts/{character_id} | Get a paginated list of contacts for a character
[**e6c013c5d20323ce7e623efa9e00f0d7()**](ContactsApi.md#e6c013c5d20323ce7e623efa9e00f0d7) | **GET** /v2/corporation/contacts/{corporation_id} | Get a list of contacts for a corporation


## `a6bb00dd1bed3f6e63b4e02c1c54ec22()`

```php
a6bb00dd1bed3f6e63b4e02c1c54ec22($character_id, $filter): \OpenAPI\Client\Model\InlineResponse2001
```

Get a paginated list of contacts for a character

Returns list of contacs

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$character_id = 56; // int | Character id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->a6bb00dd1bed3f6e63b4e02c1c54ec22($character_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->a6bb00dd1bed3f6e63b4e02c1c54ec22: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character_id** | **int**| Character id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `e6c013c5d20323ce7e623efa9e00f0d7()`

```php
e6c013c5d20323ce7e623efa9e00f0d7($corporation_id, $filter): \OpenAPI\Client\Model\InlineResponse20015
```

Get a list of contacts for a corporation

Returns a list of contacts

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$corporation_id = 56; // int | Corporation id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->e6c013c5d20323ce7e623efa9e00f0d7($corporation_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->e6c013c5d20323ce7e623efa9e00f0d7: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporation_id** | **int**| Corporation id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse20015**](../Model/InlineResponse20015.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

# OpenAPI\Client\ContractsApi

All URIs are relative to https://seat.services.wormholerbtw.space/api.

Method | HTTP request | Description
------------- | ------------- | -------------
[**call4d3c61c1fb4e8281c4e4b02aab77feec()**](ContractsApi.md#call4d3c61c1fb4e8281c4e4b02aab77feec) | **GET** /v2/corporation/contracts/{corporation_id} | Get a paginated list of contracts for a corporation
[**call76a3daa992b27d8522688f16deac819e()**](ContractsApi.md#call76a3daa992b27d8522688f16deac819e) | **GET** /v2/character/contracts/{character_id} | Get a paginated list of contracts for a character


## `call4d3c61c1fb4e8281c4e4b02aab77feec()`

```php
call4d3c61c1fb4e8281c4e4b02aab77feec($corporation_id, $filter): \OpenAPI\Client\Model\InlineResponse2002
```

Get a paginated list of contracts for a corporation

Returns a list of contracts

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ContractsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$corporation_id = 56; // int | Corporation id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->call4d3c61c1fb4e8281c4e4b02aab77feec($corporation_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContractsApi->call4d3c61c1fb4e8281c4e4b02aab77feec: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporation_id** | **int**| Corporation id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call76a3daa992b27d8522688f16deac819e()`

```php
call76a3daa992b27d8522688f16deac819e($character_id, $filter): \OpenAPI\Client\Model\InlineResponse2002
```

Get a paginated list of contracts for a character

Returns list of contracts

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ContractsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$character_id = 56; // int | Character id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->call76a3daa992b27d8522688f16deac819e($character_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContractsApi->call76a3daa992b27d8522688f16deac819e: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character_id** | **int**| Character id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

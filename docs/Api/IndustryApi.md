# OpenAPI\Client\IndustryApi

All URIs are relative to https://seat.services.wormholerbtw.space/api.

Method | HTTP request | Description
------------- | ------------- | -------------
[**call7338af63f38cdbfa0e6634f882758ebe()**](IndustryApi.md#call7338af63f38cdbfa0e6634f882758ebe) | **GET** /v2/corporation/industry/{corporation_id} | Get a paginated list of industry jobs for a corporation
[**df86905de68b4f55a7c767a07adf5692()**](IndustryApi.md#df86905de68b4f55a7c767a07adf5692) | **GET** /v2/character/industry/{character_id} | Get a paginated list of industry jobs for a character


## `call7338af63f38cdbfa0e6634f882758ebe()`

```php
call7338af63f38cdbfa0e6634f882758ebe($corporation_id, $filter): \OpenAPI\Client\Model\InlineResponse20016
```

Get a paginated list of industry jobs for a corporation

Returns a list of industry jobs

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\IndustryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$corporation_id = 56; // int | Corporation id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->call7338af63f38cdbfa0e6634f882758ebe($corporation_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndustryApi->call7338af63f38cdbfa0e6634f882758ebe: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporation_id** | **int**| Corporation id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse20016**](../Model/InlineResponse20016.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `df86905de68b4f55a7c767a07adf5692()`

```php
df86905de68b4f55a7c767a07adf5692($character_id, $filter): \OpenAPI\Client\Model\InlineResponse2004
```

Get a paginated list of industry jobs for a character

Returns list of industry jobs

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\IndustryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$character_id = 56; // int | Character id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->df86905de68b4f55a7c767a07adf5692($character_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndustryApi->df86905de68b4f55a7c767a07adf5692: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character_id** | **int**| Character id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

# OpenAPI\Client\CharacterApi

All URIs are relative to https://seat.services.wormholerbtw.space/api.

Method | HTTP request | Description
------------- | ------------- | -------------
[**adad9bd5091da85471977d8dc17239e6()**](CharacterApi.md#adad9bd5091da85471977d8dc17239e6) | **GET** /v2/character/jump-clones/{character_id} | Get a paginated list of jump clones for a character
[**adf0d485f7c96327108d9aad6996c1dd()**](CharacterApi.md#adf0d485f7c96327108d9aad6996c1dd) | **GET** /v2/character/notifications/{character_id} | Get a paginated list of notifications for a character
[**call0dc54bd4de18e00e8d3cb04f9132ffc0()**](CharacterApi.md#call0dc54bd4de18e00e8d3cb04f9132ffc0) | **GET** /v2/character/sheet/{character_id} | Get the character sheet for a character
[**call3d43bc5a8d462e1a335f687a9f20ff2e()**](CharacterApi.md#call3d43bc5a8d462e1a335f687a9f20ff2e) | **GET** /v2/character/mail/{character_id} | Get a paginated list of mail for a character
[**cea7975e72cc2678840f5bcfc20aa45c()**](CharacterApi.md#cea7975e72cc2678840f5bcfc20aa45c) | **GET** /v2/character/skill-queue/{character_id} | Get a list of characters skill queue
[**cf85d2d86997fa5650cc4e927d3283ee()**](CharacterApi.md#cf85d2d86997fa5650cc4e927d3283ee) | **GET** /v2/character/corporation-history/{character_id} | Get the corporation history for a character
[**f71b7adb5274009dad7792fe95fb7e07()**](CharacterApi.md#f71b7adb5274009dad7792fe95fb7e07) | **GET** /v2/character/skills/{character_id} | Get the skills for a character


## `adad9bd5091da85471977d8dc17239e6()`

```php
adad9bd5091da85471977d8dc17239e6($character_id, $filter): \OpenAPI\Client\Model\InlineResponse2005
```

Get a paginated list of jump clones for a character

Returns list of jump clones

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CharacterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$character_id = 56; // int | Character id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->adad9bd5091da85471977d8dc17239e6($character_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CharacterApi->adad9bd5091da85471977d8dc17239e6: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character_id** | **int**| Character id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse2005**](../Model/InlineResponse2005.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `adf0d485f7c96327108d9aad6996c1dd()`

```php
adf0d485f7c96327108d9aad6996c1dd($character_id, $filter): \OpenAPI\Client\Model\InlineResponse2008
```

Get a paginated list of notifications for a character

Returns a list of notifications

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CharacterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$character_id = 56; // int | Character id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->adf0d485f7c96327108d9aad6996c1dd($character_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CharacterApi->adf0d485f7c96327108d9aad6996c1dd: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character_id** | **int**| Character id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse2008**](../Model/InlineResponse2008.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call0dc54bd4de18e00e8d3cb04f9132ffc0()`

```php
call0dc54bd4de18e00e8d3cb04f9132ffc0($character_id): \OpenAPI\Client\Model\InlineResponse2009
```

Get the character sheet for a character

Returns a character sheet

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CharacterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$character_id = 56; // int | Character id

try {
    $result = $apiInstance->call0dc54bd4de18e00e8d3cb04f9132ffc0($character_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CharacterApi->call0dc54bd4de18e00e8d3cb04f9132ffc0: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character_id** | **int**| Character id |

### Return type

[**\OpenAPI\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call3d43bc5a8d462e1a335f687a9f20ff2e()`

```php
call3d43bc5a8d462e1a335f687a9f20ff2e($character_id, $filter): \OpenAPI\Client\Model\InlineResponse2006
```

Get a paginated list of mail for a character

Returns mail

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CharacterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$character_id = 56; // int | Character id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->call3d43bc5a8d462e1a335f687a9f20ff2e($character_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CharacterApi->call3d43bc5a8d462e1a335f687a9f20ff2e: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character_id** | **int**| Character id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse2006**](../Model/InlineResponse2006.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `cea7975e72cc2678840f5bcfc20aa45c()`

```php
cea7975e72cc2678840f5bcfc20aa45c($character_id, $filter): \OpenAPI\Client\Model\InlineResponse20011
```

Get a list of characters skill queue

Returns a skill queue

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CharacterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$character_id = 56; // int | Character id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->cea7975e72cc2678840f5bcfc20aa45c($character_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CharacterApi->cea7975e72cc2678840f5bcfc20aa45c: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character_id** | **int**| Character id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse20011**](../Model/InlineResponse20011.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `cf85d2d86997fa5650cc4e927d3283ee()`

```php
cf85d2d86997fa5650cc4e927d3283ee($character_id, $filter): \OpenAPI\Client\Model\InlineResponse2003
```

Get the corporation history for a character

Returns a corporation history

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CharacterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$character_id = 56; // int | Character id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->cf85d2d86997fa5650cc4e927d3283ee($character_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CharacterApi->cf85d2d86997fa5650cc4e927d3283ee: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character_id** | **int**| Character id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `f71b7adb5274009dad7792fe95fb7e07()`

```php
f71b7adb5274009dad7792fe95fb7e07($character_id, $filter): \OpenAPI\Client\Model\InlineResponse20010
```

Get the skills for a character

Returns character skills

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CharacterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$character_id = 56; // int | Character id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->f71b7adb5274009dad7792fe95fb7e07($character_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CharacterApi->f71b7adb5274009dad7792fe95fb7e07: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character_id** | **int**| Character id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse20010**](../Model/InlineResponse20010.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

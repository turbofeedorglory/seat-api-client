# OpenAPI\Client\RolesApi

All URIs are relative to https://seat.services.wormholerbtw.space/api.

Method | HTTP request | Description
------------- | ------------- | -------------
[**a9c2d03b01bc2138f0826b23f5a980ca()**](RolesApi.md#a9c2d03b01bc2138f0826b23f5a980ca) | **PATCH** /v2/roles/{role_id} | Edit an existing SeAT role
[**b9951e9b42d41515a9bf73bd43bc5ab7()**](RolesApi.md#b9951e9b42d41515a9bf73bd43bc5ab7) | **POST** /v2/roles | Create a new SeAT role
[**call559745b6fccf8de1570962d8c492f229()**](RolesApi.md#call559745b6fccf8de1570962d8c492f229) | **DELETE** /v2/roles/members/{user_id}/{role_id} | Revoke a SeAT role from an user
[**call789426941aed7d332b63ff5f9a1d3e77()**](RolesApi.md#call789426941aed7d332b63ff5f9a1d3e77) | **DELETE** /v2/roles/{role_id} | Delete a SeAT role
[**call85121e5fae265f8c66a39d3f47441a59()**](RolesApi.md#call85121e5fae265f8c66a39d3f47441a59) | **GET** /v2/roles | Get the roles configured within SeAT
[**call92356addbf79c2a7d3a6e376adfc05b4()**](RolesApi.md#call92356addbf79c2a7d3a6e376adfc05b4) | **GET** /v2/roles/{role_id} | Get detailed information about a role
[**e7597dfde6abe556b8f20f4f4f7f5ea1()**](RolesApi.md#e7597dfde6abe556b8f20f4f4f7f5ea1) | **POST** /v2/roles/members | Grant a user a SeAT role
[**ea8d2bdbf03712cf2eedee524a9c831f()**](RolesApi.md#ea8d2bdbf03712cf2eedee524a9c831f) | **GET** /v2/roles/query/permission-check/{user_id}/{permission_name} | Check if a user has a role
[**f47790627b1f040eae7e92a89f17dfc5()**](RolesApi.md#f47790627b1f040eae7e92a89f17dfc5) | **GET** /v2/roles/query/permissions | Get the available SeAT permissions
[**f57ba37792c2fcbf0e7805134528ac80()**](RolesApi.md#f57ba37792c2fcbf0e7805134528ac80) | **GET** /v2/roles/query/role-check/{user_id}/{role_name} | Check if a user has a role


## `a9c2d03b01bc2138f0826b23f5a980ca()`

```php
a9c2d03b01bc2138f0826b23f5a980ca($role_id, $inline_object1): \OpenAPI\Client\Model\InlineResponse20025
```

Edit an existing SeAT role

Edit a role

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RolesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$role_id = 56; // int | Role ID
$inline_object1 = new \OpenAPI\Client\Model\InlineObject1(); // \OpenAPI\Client\Model\InlineObject1

try {
    $result = $apiInstance->a9c2d03b01bc2138f0826b23f5a980ca($role_id, $inline_object1);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RolesApi->a9c2d03b01bc2138f0826b23f5a980ca: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_id** | **int**| Role ID |
 **inline_object1** | [**\OpenAPI\Client\Model\InlineObject1**](../Model/InlineObject1.md)|  | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse20025**](../Model/InlineResponse20025.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `b9951e9b42d41515a9bf73bd43bc5ab7()`

```php
b9951e9b42d41515a9bf73bd43bc5ab7($inline_object): \OpenAPI\Client\Model\InlineResponse201
```

Create a new SeAT role

Creates a role

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RolesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$inline_object = new \OpenAPI\Client\Model\InlineObject(); // \OpenAPI\Client\Model\InlineObject

try {
    $result = $apiInstance->b9951e9b42d41515a9bf73bd43bc5ab7($inline_object);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RolesApi->b9951e9b42d41515a9bf73bd43bc5ab7: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object** | [**\OpenAPI\Client\Model\InlineObject**](../Model/InlineObject.md)|  | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse201**](../Model/InlineResponse201.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call559745b6fccf8de1570962d8c492f229()`

```php
call559745b6fccf8de1570962d8c492f229($user_id, $role_id)
```

Revoke a SeAT role from an user

Revokes a role

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RolesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user_id = 56; // int | User identifier
$role_id = 56; // int | Role id

try {
    $apiInstance->call559745b6fccf8de1570962d8c492f229($user_id, $role_id);
} catch (Exception $e) {
    echo 'Exception when calling RolesApi->call559745b6fccf8de1570962d8c492f229: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| User identifier |
 **role_id** | **int**| Role id |

### Return type

void (empty response body)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call789426941aed7d332b63ff5f9a1d3e77()`

```php
call789426941aed7d332b63ff5f9a1d3e77($role_id)
```

Delete a SeAT role

Deletes a role

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RolesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$role_id = 56; // int | Role id

try {
    $apiInstance->call789426941aed7d332b63ff5f9a1d3e77($role_id);
} catch (Exception $e) {
    echo 'Exception when calling RolesApi->call789426941aed7d332b63ff5f9a1d3e77: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_id** | **int**| Role id |

### Return type

void (empty response body)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call85121e5fae265f8c66a39d3f47441a59()`

```php
call85121e5fae265f8c66a39d3f47441a59(): \OpenAPI\Client\Model\InlineResponse20024
```

Get the roles configured within SeAT

Returns a list of roles

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RolesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->call85121e5fae265f8c66a39d3f47441a59();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RolesApi->call85121e5fae265f8c66a39d3f47441a59: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\OpenAPI\Client\Model\InlineResponse20024**](../Model/InlineResponse20024.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call92356addbf79c2a7d3a6e376adfc05b4()`

```php
call92356addbf79c2a7d3a6e376adfc05b4($role_id): \OpenAPI\Client\Model\InlineResponse20025
```

Get detailed information about a role

Returns a roles details

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RolesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$role_id = 56; // int | Role id

try {
    $result = $apiInstance->call92356addbf79c2a7d3a6e376adfc05b4($role_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RolesApi->call92356addbf79c2a7d3a6e376adfc05b4: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_id** | **int**| Role id |

### Return type

[**\OpenAPI\Client\Model\InlineResponse20025**](../Model/InlineResponse20025.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `e7597dfde6abe556b8f20f4f4f7f5ea1()`

```php
e7597dfde6abe556b8f20f4f4f7f5ea1($inline_object2)
```

Grant a user a SeAT role

Grants a role

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RolesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$inline_object2 = new \OpenAPI\Client\Model\InlineObject2(); // \OpenAPI\Client\Model\InlineObject2

try {
    $apiInstance->e7597dfde6abe556b8f20f4f4f7f5ea1($inline_object2);
} catch (Exception $e) {
    echo 'Exception when calling RolesApi->e7597dfde6abe556b8f20f4f4f7f5ea1: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object2** | [**\OpenAPI\Client\Model\InlineObject2**](../Model/InlineObject2.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `ea8d2bdbf03712cf2eedee524a9c831f()`

```php
ea8d2bdbf03712cf2eedee524a9c831f($user_id, $permission_name)
```

Check if a user has a role

Returns a boolean

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RolesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user_id = 56; // int | User id
$permission_name = 'permission_name_example'; // string | SeAT Permission name

try {
    $apiInstance->ea8d2bdbf03712cf2eedee524a9c831f($user_id, $permission_name);
} catch (Exception $e) {
    echo 'Exception when calling RolesApi->ea8d2bdbf03712cf2eedee524a9c831f: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| User id |
 **permission_name** | **string**| SeAT Permission name |

### Return type

void (empty response body)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `f47790627b1f040eae7e92a89f17dfc5()`

```php
f47790627b1f040eae7e92a89f17dfc5(): \OpenAPI\Client\Model\InlineResponse20026
```

Get the available SeAT permissions

Returns a list of permissions

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RolesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->f47790627b1f040eae7e92a89f17dfc5();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RolesApi->f47790627b1f040eae7e92a89f17dfc5: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\OpenAPI\Client\Model\InlineResponse20026**](../Model/InlineResponse20026.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `f57ba37792c2fcbf0e7805134528ac80()`

```php
f57ba37792c2fcbf0e7805134528ac80($user_id, $role_name)
```

Check if a user has a role

Returns a boolean

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RolesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user_id = 56; // int | User id
$role_name = 'role_name_example'; // string | SeAT Role name

try {
    $apiInstance->f57ba37792c2fcbf0e7805134528ac80($user_id, $role_name);
} catch (Exception $e) {
    echo 'Exception when calling RolesApi->f57ba37792c2fcbf0e7805134528ac80: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| User id |
 **role_name** | **string**| SeAT Role name |

### Return type

void (empty response body)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

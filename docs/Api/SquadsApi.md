# OpenAPI\Client\SquadsApi

All URIs are relative to https://seat.services.wormholerbtw.space/api.

Method | HTTP request | Description
------------- | ------------- | -------------
[**call37f22499ac5a50ccaffbd6427863e402()**](SquadsApi.md#call37f22499ac5a50ccaffbd6427863e402) | **POST** /v2/squads/ | Create a new SeAT squad
[**call61888b93ce0ecc350c7969bd702f866f()**](SquadsApi.md#call61888b93ce0ecc350c7969bd702f866f) | **DELETE** /v2/squads/{squad_id} | Delete a SeAT squad
[**call81efae22f6e7b401fc434f2f30a0f139()**](SquadsApi.md#call81efae22f6e7b401fc434f2f30a0f139) | **GET** /v2/squads/{squad_id} | Get details about a Squad
[**eb59ebfbde482a6d8d38dc933fa25a54()**](SquadsApi.md#eb59ebfbde482a6d8d38dc933fa25a54) | **GET** /v2/squads | Get a list of squads


## `call37f22499ac5a50ccaffbd6427863e402()`

```php
call37f22499ac5a50ccaffbd6427863e402($inline_object3): \OpenAPI\Client\Model\InlineResponse20029
```

Create a new SeAT squad

Creates a new SeAT Squad

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\SquadsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$inline_object3 = new \OpenAPI\Client\Model\InlineObject3(); // \OpenAPI\Client\Model\InlineObject3

try {
    $result = $apiInstance->call37f22499ac5a50ccaffbd6427863e402($inline_object3);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SquadsApi->call37f22499ac5a50ccaffbd6427863e402: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object3** | [**\OpenAPI\Client\Model\InlineObject3**](../Model/InlineObject3.md)|  | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse20029**](../Model/InlineResponse20029.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call61888b93ce0ecc350c7969bd702f866f()`

```php
call61888b93ce0ecc350c7969bd702f866f($squad_id)
```

Delete a SeAT squad

Deletes a squad

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\SquadsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$squad_id = 56; // int | A SeAT squad id

try {
    $apiInstance->call61888b93ce0ecc350c7969bd702f866f($squad_id);
} catch (Exception $e) {
    echo 'Exception when calling SquadsApi->call61888b93ce0ecc350c7969bd702f866f: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **squad_id** | **int**| A SeAT squad id |

### Return type

void (empty response body)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call81efae22f6e7b401fc434f2f30a0f139()`

```php
call81efae22f6e7b401fc434f2f30a0f139($squad_id): \OpenAPI\Client\Model\InlineResponse20028
```

Get details about a Squad

Return detailled information from a Squad

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\SquadsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$squad_id = 56; // int | Squad id

try {
    $result = $apiInstance->call81efae22f6e7b401fc434f2f30a0f139($squad_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SquadsApi->call81efae22f6e7b401fc434f2f30a0f139: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **squad_id** | **int**| Squad id |

### Return type

[**\OpenAPI\Client\Model\InlineResponse20028**](../Model/InlineResponse20028.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `eb59ebfbde482a6d8d38dc933fa25a54()`

```php
eb59ebfbde482a6d8d38dc933fa25a54(): \OpenAPI\Client\Model\InlineResponse20027
```

Get a list of squads

Returns list of squads

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\SquadsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->eb59ebfbde482a6d8d38dc933fa25a54();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SquadsApi->eb59ebfbde482a6d8d38dc933fa25a54: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\OpenAPI\Client\Model\InlineResponse20027**](../Model/InlineResponse20027.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

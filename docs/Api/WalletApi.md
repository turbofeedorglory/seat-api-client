# OpenAPI\Client\WalletApi

All URIs are relative to https://seat.services.wormholerbtw.space/api.

Method | HTTP request | Description
------------- | ------------- | -------------
[**call52705c999d0f698826008008d3cd5e52()**](WalletApi.md#call52705c999d0f698826008008d3cd5e52) | **GET** /v2/character/wallet-journal/{character_id} | Get a paginated wallet journal for a character
[**ec863a5097acd47cf0dbd89eb95f2d66()**](WalletApi.md#ec863a5097acd47cf0dbd89eb95f2d66) | **GET** /v2/corporation/wallet-journal/{corporation_id} | Get a paginated wallet journal for a corporation
[**f821e55ea12d5251adb90b240c8de8ca()**](WalletApi.md#f821e55ea12d5251adb90b240c8de8ca) | **GET** /v2/corporation/wallet-transactions/{corporation_id} | Get paginated wallet transactions for a corporation
[**fffdcea91aa35b1ec912355a4a7f312d()**](WalletApi.md#fffdcea91aa35b1ec912355a4a7f312d) | **GET** /v2/character/wallet-transactions/{character_id} | Get paginated wallet transactions for a character


## `call52705c999d0f698826008008d3cd5e52()`

```php
call52705c999d0f698826008008d3cd5e52($character_id, $filter): \OpenAPI\Client\Model\InlineResponse20012
```

Get a paginated wallet journal for a character

Returns a wallet journal

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\WalletApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$character_id = 56; // int | Character id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->call52705c999d0f698826008008d3cd5e52($character_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WalletApi->call52705c999d0f698826008008d3cd5e52: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character_id** | **int**| Character id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse20012**](../Model/InlineResponse20012.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `ec863a5097acd47cf0dbd89eb95f2d66()`

```php
ec863a5097acd47cf0dbd89eb95f2d66($corporation_id, $filter): \OpenAPI\Client\Model\InlineResponse20020
```

Get a paginated wallet journal for a corporation

Returns a wallet journal

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\WalletApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$corporation_id = 56; // int | Corporation id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->ec863a5097acd47cf0dbd89eb95f2d66($corporation_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WalletApi->ec863a5097acd47cf0dbd89eb95f2d66: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporation_id** | **int**| Corporation id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse20020**](../Model/InlineResponse20020.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `f821e55ea12d5251adb90b240c8de8ca()`

```php
f821e55ea12d5251adb90b240c8de8ca($corporation_id, $filter): \OpenAPI\Client\Model\InlineResponse20021
```

Get paginated wallet transactions for a corporation

Returns wallet transactions

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\WalletApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$corporation_id = 56; // int | Corporation id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->f821e55ea12d5251adb90b240c8de8ca($corporation_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WalletApi->f821e55ea12d5251adb90b240c8de8ca: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporation_id** | **int**| Corporation id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse20021**](../Model/InlineResponse20021.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `fffdcea91aa35b1ec912355a4a7f312d()`

```php
fffdcea91aa35b1ec912355a4a7f312d($character_id, $filter): \OpenAPI\Client\Model\InlineResponse20013
```

Get paginated wallet transactions for a character

Returns wallet transactions

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\WalletApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$character_id = 56; // int | Character id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->fffdcea91aa35b1ec912355a4a7f312d($character_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WalletApi->fffdcea91aa35b1ec912355a4a7f312d: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character_id** | **int**| Character id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse20013**](../Model/InlineResponse20013.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

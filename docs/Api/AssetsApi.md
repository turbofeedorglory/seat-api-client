# OpenAPI\Client\AssetsApi

All URIs are relative to https://seat.services.wormholerbtw.space/api.

Method | HTTP request | Description
------------- | ------------- | -------------
[**call4c8adfa8ef264474d5d6bdcb07a08e61()**](AssetsApi.md#call4c8adfa8ef264474d5d6bdcb07a08e61) | **GET** /v2/corporation/assets/{corporation_id} | Get a paginated list of a assets for a corporation
[**call9cf9b486e75de62ca10b96cf44bc72be()**](AssetsApi.md#call9cf9b486e75de62ca10b96cf44bc72be) | **GET** /v2/character/assets/{character_id} | Get a paginated list of a assets for a character


## `call4c8adfa8ef264474d5d6bdcb07a08e61()`

```php
call4c8adfa8ef264474d5d6bdcb07a08e61($corporation_id, $filter): \OpenAPI\Client\Model\InlineResponse20014
```

Get a paginated list of a assets for a corporation

Returns a list of assets

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\AssetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$corporation_id = 56; // int | Corporation id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->call4c8adfa8ef264474d5d6bdcb07a08e61($corporation_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AssetsApi->call4c8adfa8ef264474d5d6bdcb07a08e61: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporation_id** | **int**| Corporation id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse20014**](../Model/InlineResponse20014.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call9cf9b486e75de62ca10b96cf44bc72be()`

```php
call9cf9b486e75de62ca10b96cf44bc72be($character_id, $filter): \OpenAPI\Client\Model\InlineResponse200
```

Get a paginated list of a assets for a character

Returns a list of assets

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\AssetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$character_id = 56; // int | Character id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->call9cf9b486e75de62ca10b96cf44bc72be($character_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AssetsApi->call9cf9b486e75de62ca10b96cf44bc72be: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character_id** | **int**| Character id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

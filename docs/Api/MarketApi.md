# OpenAPI\Client\MarketApi

All URIs are relative to https://seat.services.wormholerbtw.space/api.

Method | HTTP request | Description
------------- | ------------- | -------------
[**call60013c0c86ff76731bb02be3eab5881d()**](MarketApi.md#call60013c0c86ff76731bb02be3eab5881d) | **GET** /v2/corporation/market-orders/{corporation_id} | Get a paginated list of market orders for a corporation
[**call6e92a0a7c4b83dd5ace6dbf3953ae353()**](MarketApi.md#call6e92a0a7c4b83dd5ace6dbf3953ae353) | **GET** /v2/character/market-orders/{character_id} | Get a paginated list of market orders for a character


## `call60013c0c86ff76731bb02be3eab5881d()`

```php
call60013c0c86ff76731bb02be3eab5881d($corporation_id, $filter): \OpenAPI\Client\Model\InlineResponse20017
```

Get a paginated list of market orders for a corporation

Returns a list of market orders

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\MarketApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$corporation_id = 56; // int | Corporation id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->call60013c0c86ff76731bb02be3eab5881d($corporation_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MarketApi->call60013c0c86ff76731bb02be3eab5881d: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporation_id** | **int**| Corporation id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse20017**](../Model/InlineResponse20017.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call6e92a0a7c4b83dd5ace6dbf3953ae353()`

```php
call6e92a0a7c4b83dd5ace6dbf3953ae353($character_id, $filter): \OpenAPI\Client\Model\InlineResponse2007
```

Get a paginated list of market orders for a character

Returns list of market orders

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\MarketApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$character_id = 56; // int | Character id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->call6e92a0a7c4b83dd5ace6dbf3953ae353($character_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MarketApi->call6e92a0a7c4b83dd5ace6dbf3953ae353: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character_id** | **int**| Character id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse2007**](../Model/InlineResponse2007.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

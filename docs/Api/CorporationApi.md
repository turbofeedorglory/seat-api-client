# OpenAPI\Client\CorporationApi

All URIs are relative to https://seat.services.wormholerbtw.space/api.

Method | HTTP request | Description
------------- | ------------- | -------------
[**call790d0d2149ca67ae7d0dc216be53baef()**](CorporationApi.md#call790d0d2149ca67ae7d0dc216be53baef) | **GET** /v2/corporation/member-tracking/{corporation_id} | Get a list of members for a corporation with tracking
[**call80868b7914d1aab73c81ddd356db45d5()**](CorporationApi.md#call80868b7914d1aab73c81ddd356db45d5) | **GET** /v2/corporation/sheet/{corporation_id} | Get a corporation sheet


## `call790d0d2149ca67ae7d0dc216be53baef()`

```php
call790d0d2149ca67ae7d0dc216be53baef($corporation_id, $filter): \OpenAPI\Client\Model\InlineResponse20018
```

Get a list of members for a corporation with tracking

Returns a list of members for a corporation

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CorporationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$corporation_id = 56; // int | Corporation id
$filter = 'filter_example'; // string | Query filter following OData format

try {
    $result = $apiInstance->call790d0d2149ca67ae7d0dc216be53baef($corporation_id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CorporationApi->call790d0d2149ca67ae7d0dc216be53baef: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporation_id** | **int**| Corporation id |
 **filter** | **string**| Query filter following OData format | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse20018**](../Model/InlineResponse20018.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call80868b7914d1aab73c81ddd356db45d5()`

```php
call80868b7914d1aab73c81ddd356db45d5($corporation_id): \OpenAPI\Client\Model\InlineResponse20019
```

Get a corporation sheet

Returns a corporation sheet

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CorporationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$corporation_id = 56; // int | Corporation id

try {
    $result = $apiInstance->call80868b7914d1aab73c81ddd356db45d5($corporation_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CorporationApi->call80868b7914d1aab73c81ddd356db45d5: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporation_id** | **int**| Corporation id |

### Return type

[**\OpenAPI\Client\Model\InlineResponse20019**](../Model/InlineResponse20019.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

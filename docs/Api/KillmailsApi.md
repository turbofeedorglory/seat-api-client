# OpenAPI\Client\KillmailsApi

All URIs are relative to https://seat.services.wormholerbtw.space/api.

Method | HTTP request | Description
------------- | ------------- | -------------
[**b8b8ef3f29bd5951ff2def26be4f002d()**](KillmailsApi.md#b8b8ef3f29bd5951ff2def26be4f002d) | **GET** /v2/killmails/{killmail_id} | Get full details about a killmail
[**call17814a7d2c1a6bb1dbe0336d41573e00()**](KillmailsApi.md#call17814a7d2c1a6bb1dbe0336d41573e00) | **GET** /v2/corporation/killmails/{corporation_id} | Get a paginated list of killmails for a corporation
[**call828598394baf5376b30bb857d49f2ead()**](KillmailsApi.md#call828598394baf5376b30bb857d49f2ead) | **GET** /v2/character/killmails/{character_id} | Get a paginated list of killmails for a character


## `b8b8ef3f29bd5951ff2def26be4f002d()`

```php
b8b8ef3f29bd5951ff2def26be4f002d($killmail_id): \OpenAPI\Client\Model\InlineResponse20023
```

Get full details about a killmail

Returns a detailed killmail

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\KillmailsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$killmail_id = 56; // int | Killmail id

try {
    $result = $apiInstance->b8b8ef3f29bd5951ff2def26be4f002d($killmail_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KillmailsApi->b8b8ef3f29bd5951ff2def26be4f002d: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **killmail_id** | **int**| Killmail id |

### Return type

[**\OpenAPI\Client\Model\InlineResponse20023**](../Model/InlineResponse20023.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call17814a7d2c1a6bb1dbe0336d41573e00()`

```php
call17814a7d2c1a6bb1dbe0336d41573e00($corporation_id): \OpenAPI\Client\Model\InlineResponse20022
```

Get a paginated list of killmails for a corporation

Returns list of killmails

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\KillmailsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$corporation_id = 56; // int | Corporation id

try {
    $result = $apiInstance->call17814a7d2c1a6bb1dbe0336d41573e00($corporation_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KillmailsApi->call17814a7d2c1a6bb1dbe0336d41573e00: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporation_id** | **int**| Corporation id |

### Return type

[**\OpenAPI\Client\Model\InlineResponse20022**](../Model/InlineResponse20022.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call828598394baf5376b30bb857d49f2ead()`

```php
call828598394baf5376b30bb857d49f2ead($character_id): \OpenAPI\Client\Model\InlineResponse20022
```

Get a paginated list of killmails for a character

Returns list of killmails

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\KillmailsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$character_id = 56; // int | Character id

try {
    $result = $apiInstance->call828598394baf5376b30bb857d49f2ead($character_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KillmailsApi->call828598394baf5376b30bb857d49f2ead: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character_id** | **int**| Character id |

### Return type

[**\OpenAPI\Client\Model\InlineResponse20022**](../Model/InlineResponse20022.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

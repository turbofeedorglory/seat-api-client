# OpenAPI\Client\UsersApi

All URIs are relative to https://seat.services.wormholerbtw.space/api.

Method | HTTP request | Description
------------- | ------------- | -------------
[**baf810a249f09694192dc4b5b4839017()**](UsersApi.md#baf810a249f09694192dc4b5b4839017) | **GET** /v2/users/{user_id} | Get group id&#39;s and associated character_id&#39;s for a user
[**call44cb5dfd60bab9966857cbe3af9aff7a()**](UsersApi.md#call44cb5dfd60bab9966857cbe3af9aff7a) | **GET** /v2/users/configured-scopes | Get a list of the scopes configured for this instance
[**call844c004c9f6492e74e1e4058340d0503()**](UsersApi.md#call844c004c9f6492e74e1e4058340d0503) | **GET** /v2/users | Get a list of users, associated character id&#39;s and group ids
[**call85aeb7c16b3694741cfc93d128e7502c()**](UsersApi.md#call85aeb7c16b3694741cfc93d128e7502c) | **POST** /v2/users/ | Create a new SeAT user
[**e422f75fa6436602f7639ff2dad5bf7a()**](UsersApi.md#e422f75fa6436602f7639ff2dad5bf7a) | **DELETE** /v2/users/{user_id} | Delete a SeAT user


## `baf810a249f09694192dc4b5b4839017()`

```php
baf810a249f09694192dc4b5b4839017($user_id): \OpenAPI\Client\Model\InlineResponse20031
```

Get group id's and associated character_id's for a user

Returns a user

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user_id = 56; // int | User id

try {
    $result = $apiInstance->baf810a249f09694192dc4b5b4839017($user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->baf810a249f09694192dc4b5b4839017: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| User id |

### Return type

[**\OpenAPI\Client\Model\InlineResponse20031**](../Model/InlineResponse20031.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call44cb5dfd60bab9966857cbe3af9aff7a()`

```php
call44cb5dfd60bab9966857cbe3af9aff7a(): string[]
```

Get a list of the scopes configured for this instance

Returns list of scopes

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->call44cb5dfd60bab9966857cbe3af9aff7a();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->call44cb5dfd60bab9966857cbe3af9aff7a: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

**string[]**

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call844c004c9f6492e74e1e4058340d0503()`

```php
call844c004c9f6492e74e1e4058340d0503(): \OpenAPI\Client\Model\InlineResponse20030
```

Get a list of users, associated character id's and group ids

Returns list of users

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->call844c004c9f6492e74e1e4058340d0503();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->call844c004c9f6492e74e1e4058340d0503: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\OpenAPI\Client\Model\InlineResponse20030**](../Model/InlineResponse20030.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `call85aeb7c16b3694741cfc93d128e7502c()`

```php
call85aeb7c16b3694741cfc93d128e7502c($inline_object4): \OpenAPI\Client\Model\InlineResponse20031
```

Create a new SeAT user

Creates a new SeAT user

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$inline_object4 = new \OpenAPI\Client\Model\InlineObject4(); // \OpenAPI\Client\Model\InlineObject4

try {
    $result = $apiInstance->call85aeb7c16b3694741cfc93d128e7502c($inline_object4);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->call85aeb7c16b3694741cfc93d128e7502c: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object4** | [**\OpenAPI\Client\Model\InlineObject4**](../Model/InlineObject4.md)|  | [optional]

### Return type

[**\OpenAPI\Client\Model\InlineResponse20031**](../Model/InlineResponse20031.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `e422f75fa6436602f7639ff2dad5bf7a()`

```php
e422f75fa6436602f7639ff2dad5bf7a($user_id)
```

Delete a SeAT user

Deletes a user

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('X-Token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('X-Token', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user_id = 56; // int | A SeAT user id

try {
    $apiInstance->e422f75fa6436602f7639ff2dad5bf7a($user_id);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->e422f75fa6436602f7639ff2dad5bf7a: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| A SeAT user id |

### Return type

void (empty response body)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

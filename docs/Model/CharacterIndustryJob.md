# # CharacterIndustryJob

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job_id** | **int** | The job ID | [optional]
**installer_id** | **int** | The character who start the job | [optional]
**facility_id** | **int** | The structure where the job has been started | [optional]
**station_id** | **int** | The outpost where the job has been started (deprecated) | [optional]
**activity_id** | **int** | The activity type used for the job | [optional]
**blueprint_id** | **int** | The item blueprint ID on which the job is based | [optional]
**blueprint_location_id** | **int** | The place where the blueprint is stored | [optional]
**output_location_id** | **int** | The place where the resulting item should be put | [optional]
**runs** | **int** | The runs amount for the activity | [optional]
**cost** | **double** | The job installation cost | [optional]
**licensed_runs** | **int** | The number of copy | [optional]
**probability** | **int** | The success rate | [optional]
**status** | **string** | The job status | [optional]
**duration** | **int** | The job duration in seconds | [optional]
**start_date** | **\DateTime** | The date-time when job has been started | [optional]
**end_date** | **\DateTime** | The date-time when job should be done | [optional]
**pause_date** | **\DateTime** | The date-time when job has been paused | [optional]
**completed_date** | **\DateTime** | The date-time when job has been delivered | [optional]
**completed_character_id** | **int** | The character who deliver the job | [optional]
**successful_runs** | **int** | The amount of completed runs | [optional]
**blueprint** | [**\OpenAPI\Client\Model\InvType**](InvType.md) |  | [optional]
**product** | [**\OpenAPI\Client\Model\InvType**](InvType.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

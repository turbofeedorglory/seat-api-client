# # CharacterContact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contact_id** | **int** | The entity ID | [optional]
**standing** | **float** | The standing between -10 and 10 | [optional]
**contact_type** | **string** | The entity type | [optional]
**is_watched** | **bool** | True if the contact is in the watchlist | [optional]
**is_blocked** | **bool** | True if the contact is in the blacklist | [optional]
**labels** | **string[]** | Labels attached to the the contact | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

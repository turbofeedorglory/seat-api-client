# # CorporationWalletTransaction

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**division** | **int** | Wallet key of the division to fetch journals from | [optional]
**transaction_id** | **int** | Unique transaction ID | [optional]
**date** | **\DateTime** | Date and time of transaction | [optional]
**location_id** | **int** | The place where the transaction has been made | [optional]
**unit_price** | **double** | Amount paid per unit | [optional]
**quantity** | **int** |  | [optional]
**is_buy** | **bool** | True if the transaction is related to a buy order | [optional]
**journal_ref_id** | **int** | -1 if there is no corresponding wallet journal entry | [optional]
**party** | [**\OpenAPI\Client\Model\UniverseName**](UniverseName.md) |  | [optional]
**type** | [**\OpenAPI\Client\Model\InvType**](InvType.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

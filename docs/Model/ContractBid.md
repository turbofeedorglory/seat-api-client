# # ContractBid

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bidder_id** | **int** | Identifier from entity who put a bid | [optional]
**date_bid** | **\DateTime** | Date/Time when the bid has been placed | [optional]
**amount** | **float** | Amount of placed bid | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InvType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type_id** | **int** | The inventory type ID | [optional]
**group_id** | **int** | The group to which the type is related | [optional]
**type_name** | **string** | The inventory type name | [optional]
**description** | **string** | The inventory type description | [optional]
**mass** | **double** | The inventory type mass | [optional]
**volume** | **double** | The inventory type volume | [optional]
**capacity** | **double** | The inventory type storage capacity | [optional]
**portion_size** | **int** |  | [optional]
**race_id** | **int** | The race to which the inventory type is tied | [optional]
**base_price** | **double** | The initial price used by NPC to create order | [optional]
**published** | **bool** | True if the item is available in-game | [optional]
**market_group_id** | **int** | The group into which the item is available on market | [optional]
**icon_id** | **int** |  | [optional]
**sound_id** | **int** |  | [optional]
**graphic_id** | **int** | Class InvType. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

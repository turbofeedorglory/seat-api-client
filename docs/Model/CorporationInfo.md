# # CorporationInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | The name of the corporation | [optional]
**ticker** | **string** | The corporation ticker name | [optional]
**member_count** | **int** | The member amount of the corporation | [optional]
**ceo** | [**\OpenAPI\Client\Model\UniverseName**](UniverseName.md) |  | [optional]
**alliance** | [**\OpenAPI\Client\Model\UniverseName**](UniverseName.md) |  | [optional]
**description** | **string** | The corporation description | [optional]
**tax_rate** | **float** | The corporation tax rate | [optional]
**date_founded** | **\DateTime** | The corporation creation date | [optional]
**creator** | [**\OpenAPI\Client\Model\UniverseName**](UniverseName.md) |  | [optional]
**url** | **string** | The corporation homepage link | [optional]
**faction** | [**\OpenAPI\Client\Model\UniverseName**](UniverseName.md) |  | [optional]
**home_station_id** | **int** | The home station where the corporation has its HQ | [optional]
**shares** | **double** | The shares attached to the corporation | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

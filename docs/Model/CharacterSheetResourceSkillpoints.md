# # CharacterSheetResourceSkillpoints

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_sp** | **float** | The total skill points owned by the character | [optional]
**unallocated_sp** | **float** | The total skill points not allocated for this character | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **string** | The new role name |
**description** | **string** | The new role description | [optional]
**logo** | **string** | Base64 encoded new role logo | [optional]
**permissions** | **string[]** | A list of the permissions which have to be attached to the role. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

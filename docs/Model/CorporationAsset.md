# # CorporationAsset

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item_id** | **int** | The item identifier | [optional]
**quantity** | **int** | The item quantity | [optional]
**location_id** | **int** | The place of the item | [optional]
**location_type** | **string** | The location qualifier | [optional]
**location_flag** | **string** | The location flag | [optional]
**is_singleton** | **bool** | True if the item is not stacked | [optional]
**x** | **double** | The x coordinate if the item is in space | [optional]
**y** | **double** | The y coordinate if the item is in space | [optional]
**z** | **double** | The z coordinate if the item is in space | [optional]
**map_id** | **int** | The map identifier into which item is located | [optional]
**map_name** | **string** | The name of the system where the item resides | [optional]
**name** | **string** | The name of the asset (ie: a ship name) | [optional]
**type** | [**\OpenAPI\Client\Model\InvType**](InvType.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InlineResponse422

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **string** | The readable error message | [optional]
**errors** | [**\OpenAPI\Client\Model\InlineResponse422Errors**](InlineResponse422Errors.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

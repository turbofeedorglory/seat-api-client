# # Squad

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The unique identifier | [optional]
**type** | **string** | The Squad management type | [optional]
**name** | **string** | The Squad name | [optional]
**logo** | **string** | The Squad Logo | [optional]
**description** | **string** | The squad description | [optional]
**roles** | **int[]** | List of roles attached to that Squad | [optional]
**moderators** | **int[]** | List of moderators attached to that Squad | [optional]
**members** | **int[]** | List of members attached to that Squad | [optional]
**applications** | **int[]** | List of candidates attached to that Squad | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

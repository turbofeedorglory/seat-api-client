# # UniverseStructure

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**structure_id** | **int** | Structure identifier | [optional]
**name** | **string** | Structure name | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

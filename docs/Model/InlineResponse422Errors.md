# # InlineResponse422Errors

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **string[]** | The field for which the error has been encountered | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

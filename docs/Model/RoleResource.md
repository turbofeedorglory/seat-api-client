# # RoleResource

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **string** | Role name | [optional]
**description** | **string** | Role description | [optional]
**logo** | **string** | Role logo | [optional]
**permissions** | [**\OpenAPI\Client\Model\PermissionResource[]**](PermissionResource.md) | Role permissions list | [optional]
**members** | **int[]** | Role members list | [optional]
**squads** | **int[]** | Role squads list | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # CorporationMemberTracking

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**character_id** | **int** | The character ID | [optional]
**start_date** | **\DateTime** | The date since which the character is member of the corporation | [optional]
**base_id** | **int** | The structure to which the main location of this character is set | [optional]
**logon_date** | **\DateTime** | The last time when we saw the character | [optional]
**logoff_date** | **\DateTime** | The last time when the character signed out | [optional]
**location_id** | **int** | The place where the character is | [optional]
**ship** | [**\OpenAPI\Client\Model\InvType**](InvType.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

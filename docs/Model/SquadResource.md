# # SquadResource

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The unique identifier | [optional]
**type** | **string** | The Squad management type | [optional]
**name** | **string** | The Squad name | [optional]
**logo** | **string** | The Squad Logo | [optional]
**description** | **string** | The squad description | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

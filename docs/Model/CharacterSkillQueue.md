# # CharacterSkillQueue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**finish_date** | **\DateTime** | The date-time when the skill training will end | [optional]
**start_date** | **\DateTime** | The date-time when the skill training start | [optional]
**finished_level** | **int** | The level at which the skill will be at end of the training | [optional]
**queue_position** | **int** | The position in the queue | [optional]
**training_start_sp** | **int** | The skillpoint amount in the skill when training start | [optional]
**level_end_sp** | **int** | The skillpoint amount earned at end of the level training | [optional]
**level_start_sp** | **int** | The skillpoint amount from which the training level is starting | [optional]
**type** | [**\OpenAPI\Client\Model\InvType**](InvType.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

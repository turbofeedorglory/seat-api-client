# # InlineObject4

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Eve Online (main) Character Name |
**active** | **bool** | Set the SeAT account state. Default is true | [optional]
**email** | **string** | A contact email address for the created user | [optional]
**main_character_id** | **int** | Eve Online main Character ID |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

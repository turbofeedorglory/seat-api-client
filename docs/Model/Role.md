# # Role

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Role unique identifier | [optional]
**title** | **string** | Role name | [optional]
**description** | **string** | Role description | [optional]
**logo** | **string** | Role logo | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # CharacterJumpClone

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**jump_clone_id** | **int** | Unique jump clone identifier | [optional]
**name** | **string** | Clone name if set | [optional]
**location_id** | **int** | The structure into which the clone resides | [optional]
**location_type** | **string** | The structure type qualifier | [optional]
**implants** | **int[]** | A list of type ID | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

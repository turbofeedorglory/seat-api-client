# # ResourcePaginatedLinks

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first** | **string** | First Page | [optional]
**last** | **string** | Last Page | [optional]
**prev** | **string** | Previous Page | [optional]
**next** | **string** | Next Page | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # MailHeader

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mail_id** | **int** | The mail identifier | [optional]
**subject** | **string** | The mail topic | [optional]
**from** | **int** | The mail sender | [optional]
**timestamp** | **\DateTime** | The date-time when the mail has been sent | [optional]
**boolean** | **bool** | True if the mail has been red | [optional]
**body** | **string** | The mail content | [optional]
**recipients** | [**\OpenAPI\Client\Model\MailRecipient[]**](MailRecipient.md) | A list of recipients | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

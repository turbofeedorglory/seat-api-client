# # KillmailDetail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**killmail_time** | **\DateTime** | The date-time when kill append | [optional]
**solar_system_id** | **int** | The solar system identifier in which the kill occurs | [optional]
**moon_id** | **int** | The moon identifier near to which the kill occurs | [optional]
**war_id** | **int** | The war identifier in which the kill involves | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

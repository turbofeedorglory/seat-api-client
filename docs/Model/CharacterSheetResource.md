# # CharacterSheetResource

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Character name | [optional]
**description** | **string** | Character biography | [optional]
**corporation** | [**\OpenAPI\Client\Model\UniverseName**](UniverseName.md) |  | [optional]
**alliance** | [**\OpenAPI\Client\Model\UniverseName**](UniverseName.md) |  | [optional]
**faction** | [**\OpenAPI\Client\Model\UniverseName**](UniverseName.md) |  | [optional]
**birthday** | **\DateTime** | Character birthday | [optional]
**gender** | **string** | Character gender | [optional]
**race_id** | **int** | Character race identifier | [optional]
**bloodline_id** | **int** | Character bloodline identifier | [optional]
**security_status** | **float** | Character security status | [optional]
**balance** | **float** | Character wallet balance | [optional]
**skillpoints** | [**\OpenAPI\Client\Model\CharacterSheetResourceSkillpoints**](CharacterSheetResourceSkillpoints.md) |  | [optional]
**user_id** | **int** | Seat user identifier | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

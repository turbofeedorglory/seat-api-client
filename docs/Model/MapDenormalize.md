# # MapDenormalize

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item_id** | **int** | The entity ID | [optional]
**type_id** | **int** | The type of the entity | [optional]
**group_id** | **int** | The group to which the entity is related | [optional]
**solar_system_id** | **int** | The system to which the entity is attached | [optional]
**constellation_id** | **int** | The constellation to which the entity is attached | [optional]
**region_id** | **int** | The region to which the entity is attached | [optional]
**orbit_id** | **int** | The orbit to which the entity is depending | [optional]
**x** | **double** | x position on the map | [optional]
**y** | **double** | y position on the map | [optional]
**z** | **double** | z position on the map | [optional]
**radius** | **double** | The radius of the entity | [optional]
**item_name** | **string** | The entity name | [optional]
**security** | **double** | The security status of the system to which entity is attached | [optional]
**celestial_index** | **int** |  | [optional]
**orbit_index** | **int** | Class MapDenormalize. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

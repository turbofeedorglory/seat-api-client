# # Killmail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**killmail_id** | **int** | The unique Killmail identifier | [optional]
**killmail_hash** | **string** | The killmail hash | [optional]
**detail** | [**\OpenAPI\Client\Model\KillmailDetail**](KillmailDetail.md) |  | [optional]
**victim** | [**\OpenAPI\Client\Model\KillmailVictim**](KillmailVictim.md) |  | [optional]
**attackers** | [**\OpenAPI\Client\Model\KillmailAttacker[]**](KillmailAttacker.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

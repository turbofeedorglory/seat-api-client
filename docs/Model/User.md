# # User

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID | [optional]
**name** | **string** | Name | [optional]
**email** | **string** | E-Mail address | [optional]
**active** | **bool** | Account status | [optional]
**last_login** | **\DateTime** | Last login to SeAT time | [optional]
**last_login_source** | **string** | Last IP address used to sign in to SeAT | [optional]
**associated_character_ids** | **int[]** | Array of attached character ID | [optional]
**main_character_id** | **int** | The main character ID of this group | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

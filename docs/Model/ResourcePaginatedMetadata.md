# # ResourcePaginatedMetadata

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_page** | **int** | The current page | [optional]
**from** | **int** | The first entity number on the page | [optional]
**last_page** | **int** | The last page available | [optional]
**path** | **string** | The base endpoint | [optional]
**per_page** | **int** | The pagination step | [optional]
**to** | **int** | The last entity number on the page | [optional]
**total** | **int** | The total of available entities | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # KillmailAttacker

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attacker_hash** | **string** | A hash composite of character_id, corporation_id, alliance_id and faction_id fields | [optional]
**character_id** | **int** | The character identifier | [optional]
**corporation_id** | **int** | The corporation identifier to which the attacker depends | [optional]
**alliance_id** | **int** | The alliance identifier to which the attacker depends | [optional]
**faction_id** | **int** | The faction identifier to which the attacker depends (if factional warfare) | [optional]
**security_status** | **float** | The attacker security status | [optional]
**final_blow** | **bool** | True if the attacker did the final blow | [optional]
**damage_done** | **int** | The amount of damage the attacker applied | [optional]
**ship_type_id** | **int** | The ship inventory type identifier into which attacker was | [optional]
**weapon_type_id** | **int** | The weapon inventory type identifier used by the attacker | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

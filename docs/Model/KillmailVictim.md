# # KillmailVictim

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**character_id** | **int** | The killed character identified | [optional]
**corporation_id** | **int** | The killed character corporation identifier | [optional]
**alliance_id** | **int** | The killed character alliance identifier (if any) | [optional]
**faction_id** | **int** | The killed character faction identifier (if factional warfare) | [optional]
**damage_taken** | **int** | The damage amount the killed character get | [optional]
**ship_type_id** | **int** | The destroyed ship inventory type identifier | [optional]
**x** | **double** | The x coordinate where the kill occurs | [optional]
**y** | **double** | The y coordinate where the kill occurs | [optional]
**z** | **double** | The z coordinate where the kill occurs | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

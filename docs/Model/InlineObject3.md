# # InlineObject3

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Squad name |
**type** | **string** | Squad type |
**description** | **string** | Squad description |
**logo** | **string** | Squad logo | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # ContractItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type_id** | **int** | The item type identifier | [optional]
**quantity** | **float** | The item quantity | [optional]
**raw_quantity** | **int** |  | [optional]
**is_singleton** | **bool** | Determine if the item is stacked | [optional]
**is_included** | **bool** | Determine if the item is contained in a parent item | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

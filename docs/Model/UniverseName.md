# # UniverseName

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity_id** | **int** | The entity identifier | [optional]
**name** | **string** | The entity name | [optional]
**category** | **string** | The entity type | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

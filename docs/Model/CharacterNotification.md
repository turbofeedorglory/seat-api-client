# # CharacterNotification

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notification_id** | **int** | The notification identifier | [optional]
**type** | **string** | The notification type | [optional]
**sender_id** | **int** | The entity who sent the notification | [optional]
**sender_type** | **string** | The sender qualifier | [optional]
**timestamp** | **\DateTime** | The date-time when notification has been sent | [optional]
**is_read** | **bool** | True if the notification has been red | [optional]
**object** | **string** | The notification content | [optional]
**created_at** | **\DateTime** | The date-time when notification has been created into SeAT | [optional]
**updated_at** | **\DateTime** | The date-time when notification has been updated into SeAT | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

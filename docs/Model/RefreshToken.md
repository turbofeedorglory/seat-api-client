# # RefreshToken

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**character_id** | **int** | Character ID to which the token is tied | [optional]
**version** | **int** | Refresh Token SSO Version | [optional]
**refresh_token** | **string** | Refresh token hash | [optional]
**scopes** | **string[]** | Scopes granted for this token | [optional]
**expires_on** | **\DateTime** | The datetime UTC when the token expires | [optional]
**token** | **string** | The short life access token | [optional]
**created_at** | **\DateTime** | The date-time when the token has been created into SeAT | [optional]
**updated_at** | **\DateTime** | The date-time when the token has been updated into SeAT | [optional]
**deleted_at** | **\DateTime** | The date-time when the token has been disabled | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # CharacterCorporationHistory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_date** | **\DateTime** | The date-time from which the character was inside the corporation | [optional]
**corporation_id** | **int** | The corporation ID into which the character was | [optional]
**is_deleted** | **bool** | True if the corporation has been close | [optional]
**record_id** | **int** | Sorting key | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

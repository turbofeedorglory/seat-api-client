# # CorporationOrder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | **int** | The market order ID | [optional]
**region_id** | **int** | The region up to which the order is valid | [optional]
**location_id** | **int** | The structure where the order is | [optional]
**range** | **int** | The range the order is covering | [optional]
**is_buy_order** | **bool** | True if the order is a buy order | [optional]
**price** | **double** | The unit price | [optional]
**volume_total** | **double** | The order initial volume | [optional]
**volume_remain** | **double** | The order remaining volume | [optional]
**issued** | **\DateTime** | The date-time when the order has been created | [optional]
**issued_by** | **int** | The entity ID who create the order | [optional]
**min_volume** | **double** | The minimum volume which is requested for a buy order | [optional]
**wallet_division** | **int** | The division to which the order is depending. | [optional]
**duration** | **int** | The number of seconds the order is valid | [optional]
**escrow** | **double** |  | [optional]
**type** | [**\OpenAPI\Client\Model\InvType**](InvType.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

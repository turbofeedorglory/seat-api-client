# # ContractDetail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contract_id** | **int** | The contract identifier | [optional]
**type** | **string** | The contract type | [optional]
**status** | **string** | The contract status | [optional]
**title** | **string** | The contract description | [optional]
**for_corporation** | **bool** | True if the contract is a corporation contract | [optional]
**availability** | **string** | The contract availability scope | [optional]
**date_issued** | **\DateTime** | The date-time when the contract has been made | [optional]
**date_expired** | **\DateTime** | The date-time when the contract is expiring | [optional]
**date_accepted** | **\DateTime** | The date-time when the contract has been accepted | [optional]
**days_to_complete** | **int** | The amount of day during which the contract is going (for courier contract) | [optional]
**date_completed** | **\DateTime** | The date-time when the contract has been completed | [optional]
**price** | **double** | The amount of ISK the acceptor entity must pay to get the contract | [optional]
**reward** | **double** | The amount of ISK the acceptor entity is earning by accepting the contract | [optional]
**collateral** | **double** | The amount of ISK the acceptor entity have to pay in case of failure | [optional]
**buyout** | **double** | The amount of ISK the contract is completed (for auction) | [optional]
**volume** | **double** | The contract volume | [optional]
**issuer** | [**\OpenAPI\Client\Model\UniverseName**](UniverseName.md) |  | [optional]
**assignee** | [**\OpenAPI\Client\Model\UniverseName**](UniverseName.md) |  | [optional]
**acceptor** | [**\OpenAPI\Client\Model\UniverseName**](UniverseName.md) |  | [optional]
**bids** | [**\OpenAPI\Client\Model\ContractBid[]**](ContractBid.md) |  | [optional]
**lines** | [**\OpenAPI\Client\Model\ContractItem[]**](ContractItem.md) |  | [optional]
**start_location** | [**\OpenAPI\Client\Model\UniverseStructure**](UniverseStructure.md) |  | [optional]
**end_location** | [**\OpenAPI\Client\Model\UniverseStructure**](UniverseStructure.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

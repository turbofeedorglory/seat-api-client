# # CharacterSkill

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**skillpoints_in_skill** | **int** | The amount of skill point actually learned for that skill | [optional]
**trained_skill_level** | **int** | The level up to which the skill as been learned | [optional]
**active_skill_level** | **int** | The level actually training | [optional]
**type** | [**\OpenAPI\Client\Model\InvType**](InvType.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

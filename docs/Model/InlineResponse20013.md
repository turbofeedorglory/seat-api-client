# # InlineResponse20013

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\OpenAPI\Client\Model\CharacterWalletTransaction[]**](CharacterWalletTransaction.md) |  | [optional]
**links** | [**\OpenAPI\Client\Model\ResourcePaginatedLinks**](ResourcePaginatedLinks.md) |  | [optional]
**meta** | [**\OpenAPI\Client\Model\ResourcePaginatedMetadata**](ResourcePaginatedMetadata.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

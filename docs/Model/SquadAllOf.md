# # SquadAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**roles** | **int[]** | List of roles attached to that Squad | [optional]
**moderators** | **int[]** | List of moderators attached to that Squad | [optional]
**members** | **int[]** | List of members attached to that Squad | [optional]
**applications** | **int[]** | List of candidates attached to that Squad | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
